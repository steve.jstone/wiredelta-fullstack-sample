import { SimpleGrid, Group, Button, Text } from '@mantine/core';
import PropTypes from "prop-types";

const Pagination = () => {
    return (
        <Group position="center" style={{marginTop: 10, marginBottom: 10}}>
            <Button variant='subtle' size="sm">Previous Page</Button>
            <Text size="sm">1/50</Text>
            <Button variant='subtle' size="sm">Next Page</Button>
        </Group>
    );
};

Pagination.propTypes = {

}

export default Pagination;