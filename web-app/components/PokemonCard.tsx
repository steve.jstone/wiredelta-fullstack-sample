import { Card, Group, Text, Badge, Button, Image, SimpleGrid } from '@mantine/core';
import PropTypes from "prop-types";

// react component
const PokemonCard = ({id, name, imageUrl, height, weight, abilities, onShowDetail, onDelete}) => {
    return (
        <Card shadow="sm" p="lg">
            <Card.Section>
                <Image src={imageUrl} height={160} alt="Pokemon Form" />
            </Card.Section>

            <Group position="center" styles={(theme)=>({ marginBottom: 20, marginTop: theme.spacing.sm })}>
                <Text size="lg" weight={600}>{name.toUpperCase()}</Text>
            </Group>

            <SimpleGrid cols={2}>
                <Text weight="bold">
                    Height
                </Text>            
                <Text>
                    {height}
                </Text>

                <Text weight="bold">
                    Weight
                </Text>            
                <Text>
                    {weight}
                </Text>

                {abilities && 
                    <>
                        <Text weight="bold">
                            Abilities
                        </Text>            
                        <Text>
                            {abilities}
                        </Text>
                    </>
                }
            </SimpleGrid>

            <Group>
                <Button variant="subtle" color="blue" style={{ marginTop: 14 }} onClick={()=>onShowDetail(id)}>
                    See details
                </Button>
                <Button variant="subtle" color="red" style={{ marginTop: 14 }} onClick={()=>onDelete(id)}>
                    Delete
                </Button>
            </Group>
      </Card>
    );
};

PokemonCard.propTypes = {

}

export default PokemonCard;