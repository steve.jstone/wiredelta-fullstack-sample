import {  Header as MantineHeader, Select, Input, Group, Button, Modal } from '@mantine/core';
import PropTypes from "prop-types";
import { useState } from "react";
import { Search, UserCircle } from 'tabler-icons-react';
import LoginForm from '../components/LoginForm';
// react component
const Header = ({page, setPage, sort, setSort}) => {

    const [showLogin, setShowLogin] = useState(false);

    const pages = [10,20,50];
    const pageOptions = pages.map(item=>({
        value: item.toString(),
        label: "Show " + item + " results"
    }));
    const sortOptions = [{
        value: "name asc", label: "From A-Z"},
        {value: "name desc", label: "From Z-A"},
        {value: "height asc", label: "By Height"},
        {value: "weight asc", label: "By Weight"}]

  return (
    <MantineHeader height={60} p="xs">         
        <Group position="center">
            <Select data={pageOptions} value={page.toString()} onChange={value=>setPage(parseInt(value))}/>
            <Input
                icon={<Search/>}
                placeholder="Search pokemon"
                />
            <Select data={sortOptions} value={sort} onChange={value=>setSort(value)}/>
            <Button rightIcon={<UserCircle />} variant="outline" onClick={()=> setShowLogin(true)}>
                Login
            </Button>
            <Modal onClose={() => setShowLogin(false)} opened={showLogin}>
               <LoginForm/>
            </Modal>
        </Group>
    </MantineHeader>
  );
};

Header.propTypes = {

}

export default Header;