import { AppShell as MantineShell, MantineProvider } from '@mantine/core';
import { useState } from "react";
import PropTypes from "prop-types";
import Header from '../components/Header';

const AppShell = ({children}) => {
  const [page, setPage] = useState(10);
  const [sort, setSort] = useState("name asc");

  return (
    <MantineProvider
      theme={{
        fontFamily: 'Verdana, sans-serif',
        fontFamilyMonospace: 'Monaco, Courier, monospace',
        headings: { fontFamily: 'Greycliff CF, sans-serif' },
      }}
    >
      <MantineShell
        padding="md"
        /*navbar={<Navbar/>}*/
        header={<Header page={page} setPage={setPage} sort={sort} setSort={setSort}/>}
        styles={(theme) => ({
          main: { backgroundColor: theme.colorScheme === 'dark' ? theme.colors.dark[8] : theme.colors.gray[0] },
        })}
      >
        {children}
      </MantineShell>
    </MantineProvider>
  );
};

AppShell.propTypes = {
  children: PropTypes.node
}

export default AppShell;