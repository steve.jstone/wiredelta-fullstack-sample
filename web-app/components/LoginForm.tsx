import { Group, Input, Button,Text, Image } from '@mantine/core';
import PropTypes from "prop-types";
import { Eye, UserCircle } from 'tabler-icons-react';

// react component
const LoginForm = () => {
    return (
    <>
      <Group position='center'>
        <UserCircle size={200} color="blue"/>
      </Group>
      <Group position='center'>
        <Text size="lg" weight="bold" style={{marginBottom: 20}}>LOGIN</Text>
      </Group>
      <Input
        placeholder="example@email.com"
        style={{marginBottom: 10}}
        />
      <Input
        icon={<Eye/>}
        placeholder="Password"
        style={{marginBottom: 10}}
        />
      <Group position='center'>
        <Button variant="outline" onClick={()=> alert('login clicked')}>
            Login
        </Button>
      </Group>
    </>
  );
};

LoginForm.propTypes = {

}

export default LoginForm;