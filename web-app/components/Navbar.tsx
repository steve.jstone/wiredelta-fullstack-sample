import { Navbar as MantineNavbar } from '@mantine/core';
import PropTypes from "prop-types";

// react component
const Navbar = () => {
    return (
    <MantineNavbar width={{ base: 300 }} height={500} p="xs">
    </MantineNavbar>
  );
};

Navbar.propTypes = {

}

export default Navbar;