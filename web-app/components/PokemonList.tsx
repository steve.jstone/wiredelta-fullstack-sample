import { SimpleGrid, Group } from '@mantine/core';
import PropTypes from "prop-types";
import Pagination from './Pagination';

// react component
const PokemonList = ({children, showPagination}) => {
    return (
        <div className="pokemon-list">
            {showPagination && <Pagination/> }
            <SimpleGrid cols={3}>{children}</SimpleGrid>
            {showPagination && <Pagination/> }
        </div>
    );
};

PokemonList.propTypes = {
    showPagination: PropTypes.bool
}

PokemonList.defaultProps = {
    showPagination: true
}

export default PokemonList;