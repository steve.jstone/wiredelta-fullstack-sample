import { NextPage, NextPageContext } from 'next';
import {  Button, Group, Image, Accordion, Text } from '@mantine/core';
import { Pokemon } from 'src/pokemon/models/pokemon.model';
import client from "../../src/utils/apollo.client";
import { gql } from "@apollo/client";
import { useEffect, useState } from "react";
import AppShell from "../../components/AppShell";
import {ChevronLeft } from 'tabler-icons-react';
import PokemonCard from 'components/PokemonCard';
import PokemonList from 'components/PokemonList';

// The component's props type
type PageProps = {
  pokemon: Pokemon;
};

// extending the default next context type
type PageContext = NextPageContext & {
  query: PageProps;
};

// react component
const Page: NextPage<PageProps> = ({pokemon}) => {  
  const [relatedPokemon, setRelatedPokemon] = useState([]);
  const { id, name, imageUrl, height, weight } = pokemon;
  const back = () =>{
    window.location.assign("/");
  }

  const onShowDetail = (id)=>{ 
    window.location.assign("/pokemon/" + id);
    };
  const onDelete = (id)=>{ alert('delete ' + id)};

  useEffect(()=>{
    const getData = async () => {
      const { data } = await client.query({
        query: gql`
          query GetAllPokemon {
            pokemons {
              id
              name
              imageUrl
              height
              weight
            }
          }
        `,
      });
      setRelatedPokemon(data.pokemons);
    }
    getData();
  },[]);

  const pokemonItems = relatedPokemon.map(item=>(<PokemonCard {...item} onShowDetail={onShowDetail} onDelete={onDelete} />));

  return (
    <AppShell>
      <Group><Button leftIcon={<ChevronLeft/>} onClick={back}>Back</Button></Group>
      <Group position='center'><Image  src={imageUrl} style={{width: 300}}/></Group>

      <Group position='center'>
        <Text>
            Height: {height.toString()}
        </Text>
        <Text>
            Weight: {weight.toString()}
        </Text>
        <Text>
            Base experience: ?
        </Text>
        <Text>
            Default: ?
        </Text>
        <Text>
            Order: ?
        </Text>
        <Text>
            Species: ?
        </Text>
      </Group>
      <Accordion>
        <Accordion.Item label="Abilities">
          ...
        </Accordion.Item>

        <Accordion.Item label="Forms">
          ...
        </Accordion.Item>

        <Accordion.Item label="Types">
          ...
        </Accordion.Item>
      </Accordion>

      <Group><Text size='lg' weight="bold" style={{marginTop: 20, marginBottom: 20}}>You Might Also Like</Text></Group>
      <PokemonList showPagination={false}>{pokemonItems}</PokemonList>
    </AppShell>
  );
};

// assigning the initial props to the component's props
Page.getInitialProps = async (ctx: PageContext) => {
  return {
    pokemon: ctx.query.pokemon,
  };
};

export default Page;