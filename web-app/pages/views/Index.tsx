import { NextPage, NextPageContext } from 'next';
import { useEffect, useState } from 'react';
import { Modal, Button } from '@mantine/core';
import { gql } from "@apollo/client";
import client from "../../src/utils/apollo.client";
import { Pokemon } from 'src/pokemon/models/pokemon.model';
import AppShell from "../../components/AppShell";
import PokemonCard from "../../components/PokemonCard";
import PokemonList from "../../components/PokemonList";

// The component's props type
type PageProps = {
  pokemon: Pokemon[];
};

// extending the default next context type
type PageContext = NextPageContext & {
  query: PageProps;
};

// react component
const Page: NextPage<PageProps> = (/*{ pokemon }*/) => {
  const [pokemon, setPokemon] = useState([]);

  const onShowDetail = (id)=>{ 
    window.location.assign("/pokemon/" + id);
    };
  const onDelete = (id)=>{ alert('delete ' + id)};

  useEffect(()=>{
    const getData = async () => {
      const { data } = await client.query({
        query: gql`
          query GetAllPokemon {
            pokemons {
              id
              name
              imageUrl
              height
              weight
            }
          }
        `,
      });
      setPokemon(data.pokemons);
    }
    getData();
  },[]);

  const pokemonItems = pokemon.map(item=>(<PokemonCard {...item} onShowDetail={onShowDetail} onDelete={onDelete} />));
  
  return (
    <AppShell>
      <PokemonList>{pokemonItems}</PokemonList>
    </AppShell>
  );
};

// assigning the initial props to the component's props
Page.getInitialProps = async (ctx: PageContext) => {
  return {
    pokemon: ctx.query.pokemon,
  };
};

export default Page;