import { PrismaClient } from '@prisma/client'
const prisma = new PrismaClient()

async function main() {
    return prisma.pokemon.findMany({
      }).then(data=> data.map(pokemon=>({
        id: pokemon.id,
        name: pokemon.name,
        imageUrl: pokemon.imageUrl,
        height: pokemon.height as unknown as number, // quick fix for now as typescript is complaining about converting Decimal to number
        weight: pokemon.weight as unknown as number
      }))).then(pokemon=>console.log(pokemon));
};

main();