import { Pokemon } from "src/pokemon/models/pokemon.model";
import { PrismaClient } from '@prisma/client'
const prisma = new PrismaClient()

const data: Pokemon[] = [
    {id: 1, name: "Bulbasaur", height: 8, weight: 69, imageUrl: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/1.png" },
    {id: 1, name: "Charmander", height: 6, weight: 85, imageUrl: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/4.png" }
]

async function main() {
    for (var pokemon of data) {
        console.log("db:seed: upserting pokemon", { pokemon })
        await prisma.pokemon.upsert({
            where: { 
                name: pokemon.name 
            },
            update: {},
            create: {
                name: pokemon.name, 
                height: pokemon.height,
                weight: pokemon.weight,
                imageUrl: pokemon.imageUrl
            }
        });
    }
};

main();