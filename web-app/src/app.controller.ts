import { Controller, Get, Res, Render, Param, UseGuards, Post, Request } from '@nestjs/common';
import { PokemonService } from './pokemon/pokemon.service';
import { AuthService } from './auth/auth.service';
import { JwtAuthGuard } from './auth/jwt-auth.guard';
import { LocalAuthGuard } from './auth/local-auth.guard';

@Controller()
export class AppController {
  constructor(
    private readonly pokemonService: PokemonService,
    private readonly authService: AuthService
  ) {}

  @Get()
  @Render('Index')
  public async index() {
    return { pokemon: await this.pokemonService.findAll({skip:0, take: 50, sortBy: "name"})};
  }

  @Get("/pokemon/:id")
  @Render('Pokemon')
  public async pokemon(@Param() {id}) {
    return { pokemon: await this.pokemonService.findOneById(parseInt(id)) };
  }

  // How to redirect in Fastify https://docs.nestjs.com/techniques/performance#performance-fastify
  @Get('/redirect')
  redirectExample(@Res() res) {
    res.status(302).redirect('/login');
  }

  @UseGuards(LocalAuthGuard)
  @Post('auth/login')
  async login(@Request() req) {
    return this.authService.login(req.user);
  }
  
  @UseGuards(JwtAuthGuard)
  @Get('profile')
  getProfile(@Request() req) {
    return req.user;
  }
}
