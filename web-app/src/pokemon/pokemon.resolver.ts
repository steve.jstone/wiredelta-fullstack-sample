import { NotFoundException, UseGuards, Injectable } from '@nestjs/common';
import { Args, Mutation, Query, Resolver, Subscription } from '@nestjs/graphql';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { NewPokemonInput } from './dto/new-pokemon.input';
import { PokemonArgs } from './dto/pokemon.args';
import { Pokemon } from './models/pokemon.model';
import { PokemonService } from './pokemon.service';

@Resolver(of => Pokemon)
//  @UseGuards(JwtAuthGuard)
export class PokemonResolver {
  constructor(private readonly pokemonsService: PokemonService) {}

  @Query(returns => Pokemon)
  async pokemon(@Args('id') id: number): Promise<Pokemon> {
    const pokemon = await this.pokemonsService.findOneById(id);
    if (!pokemon) {
      throw new NotFoundException(id);
    }
    return pokemon;
  }

  @Query(returns => [Pokemon])
  pokemons(@Args() pokemonsArgs: PokemonArgs): Promise<Pokemon[]> {
    return this.pokemonsService.findAll(pokemonsArgs);
  }

  @Mutation(returns => Pokemon)
  async addPokemon(
    @Args('newPokemonData') newPokemonData: NewPokemonInput,
  ): Promise<Pokemon> {
    const pokemon = await this.pokemonsService.create(newPokemonData);
    return pokemon;
  }

  @Mutation(returns => Boolean)
  async removePokemon(@Args('id') id: number) {
    return this.pokemonsService.remove(id);
  }
}