import { Field, InputType } from '@nestjs/graphql';
import { Length, MaxLength } from 'class-validator';

@InputType()
export class NewPokemonInput {
  @Field()
  @MaxLength(30)
  name: string;

  @Field()
  @Length(30, 255)
  imageUrl: string;

  @Field()
  height: number;

  @Field()
  weight: number;
}