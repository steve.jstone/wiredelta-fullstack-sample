import { ArgsType, Field, Int } from '@nestjs/graphql';
import { IsIn, Min, IsOptional } from 'class-validator';

@ArgsType()
export class PokemonArgs {
  @Field(type => Int)
  @Min(0)
  skip = 0;

  @Field(type => Int)
  @IsIn([10,20,50])
  take = 10;

  @Field(type => String)
  @IsIn(["height", "weight", "name"])
  sortBy = "name";

  @Field(type => String)
  @IsOptional()
  name? = null;

  @Field(type => Int)
  @IsOptional()
  minHeight? = null;

  @Field(type => Int)
  @IsOptional()
  maxHeight? = null;

  @Field(type => Int)
  @IsOptional()
  minWeight? = null;

  @Field(type => Int)
  @IsOptional()
  maxWeight? = null;
}