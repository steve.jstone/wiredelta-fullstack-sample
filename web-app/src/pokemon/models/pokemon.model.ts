import { Field, ID, ObjectType } from '@nestjs/graphql';

@ObjectType({ description: 'pokemon ' })
export class Pokemon {
  @Field(type => ID)
  id: number;

  @Field()
  name: string;

  @Field()
  imageUrl: string;

  @Field()
  height: number;

  @Field()
  weight: number;
}