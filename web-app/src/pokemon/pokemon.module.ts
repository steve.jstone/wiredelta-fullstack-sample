import { Module } from '@nestjs/common';
import { AuthModule } from 'src/auth/auth.module';
import { PokemonResolver } from './pokemon.resolver';
import { PokemonService } from './pokemon.service';

@Module({
  imports: [AuthModule],
  providers: [PokemonResolver, PokemonService],
})
export class PokemonModule {}