import { Injectable } from '@nestjs/common';
import { NewPokemonInput } from './dto/new-pokemon.input';
import { PokemonArgs } from './dto/pokemon.args';
import { Pokemon } from './models/pokemon.model';
import prismaClient from "../utils/prisma.client";
import { Decimal } from '@prisma/client/runtime';

/**
 * Maps from the database Pokemon type to the GraphQL Pokemon type
 * @param pokemon 
 * @returns 
 */
const mapPokemon = pokemon =>({
  id: pokemon.id,
  name: pokemon.name,
  imageUrl: pokemon.imageUrl,
  height: pokemon.height as unknown as number,
  weight: pokemon.weight as unknown as number
});

@Injectable()
export class PokemonService {
  async create(data: NewPokemonInput): Promise<Pokemon> {
    return prismaClient.pokemon.upsert({
        where: { 
            name: data.name 
        },
        update: {},
        create: {
            name: data.name, 
            height: data.height as unknown as Decimal,
            weight: data.weight as unknown as Decimal,
            imageUrl: data.imageUrl
        }
    }).then(mapPokemon);
  }

  async findOneById(id: number): Promise<Pokemon> {
    return await prismaClient.pokemon.findUnique({
      where: { id }
    }).then(mapPokemon);
  }

  async findAll(pokemonArgs: PokemonArgs): Promise<Pokemon[]> {

    const where : any = {};
    if (pokemonArgs.name) where.name = {contains: pokemonArgs.name};

    const orderBy: any = { };
    switch (pokemonArgs.sortBy) {
      case "name":
        orderBy.name = "asc";
        break;
      case "weight":
        orderBy.weight = "asc";
        break;
      case "height":
        orderBy.height = "asc";
        break;
      default:
        orderBy.name = "asc";
        break;
    }

    return await prismaClient.pokemon.findMany({
      skip: pokemonArgs.skip, 
      take: pokemonArgs.take,
      where,
      orderBy
    }).then(data=> data.map(mapPokemon));
  }

  async remove(id: number): Promise<boolean> {
    return await prismaClient.pokemon.delete({where: { id }}).then(_=>true).catch(_=>false);
  }
}