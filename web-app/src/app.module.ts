import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql'
import { AppController } from './app.controller';
import { join } from 'path';
import { ApolloDriver, ApolloDriverConfig } from '@nestjs/apollo';
import { ApolloServerPluginLandingPageLocalDefault } from 'apollo-server-core';
import { AppService } from './app.service';
import Next from 'next';
import { RenderModule } from 'nest-next';
import { PokemonModule } from './pokemon/pokemon.module';
import { PokemonService } from './pokemon/pokemon.service';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { AuthService } from './auth/auth.service';

@Module({
  imports: [
    PokemonModule,
    RenderModule.forRootAsync(Next({ dev: process.env.NODE_ENV !== 'production' })),
    GraphQLModule.forRoot<ApolloDriverConfig>({
      driver: ApolloDriver,
      autoSchemaFile: join(process.cwd(), 'src/schema.gql'),
      playground: false,
      plugins: [ApolloServerPluginLandingPageLocalDefault()],
    }),
    AuthModule
  ],
  controllers: [AppController],
  providers: [AppService, PokemonService],
})
export class AppModule {}
