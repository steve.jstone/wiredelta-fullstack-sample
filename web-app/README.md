
# WireDelta Pokemon Manager

## Getting Started

You will need to have docker installed on your system.

- To install the dependencies run `yarn`.
- `yarn stack` will run the database server.
- `yarn db:migrate` will add the database migrations.
- `yarn db:seed` will seed the database with some default values.
- `yarn start` will run the application server.

Go to http://localhost:3000 and enjoy the glorious Pokemon.

You can also go to http://localhost:3000/graphql for the GraphQL endpoint.

The file `auth-test.http` has sample requests to test the authentication. `/auth/login` can be sent a `{"username": "john", "password": "changeme"}` and will return a bearer token which can be used for the `/profile` page.

## Useful development commands

- `npx prisma migrate dev --name {migration-name}`